#include "apache_gsoap.h"
#include "calc.nsmap"
#include "soapcalcService.h"
IMPLEMENT_GSOAP_SERVER()
extern "C" int soap_serve(struct soap *soap) {
  calcService service(soap);
  int err = service.serve();
  service.destroy();
  return err;
}
int calcService::add(double a, double b, double &result) {
  result = a + b;
  return SOAP_OK;
}
int calcService::sub(double a, double b, double &result) {
  result = a - b;
  return SOAP_OK;
}
int calcService::mul(double a, double b, double &result) {
  result = a * b;
  return SOAP_OK;
}
int calcService::div(double a, double b, double &result) {
  result = b != 0 ? a / b : 0.0;
  return SOAP_OK;
}
int calcService::pow(double a, double b, double &result) {
  result = ::pow(a, b);
  return SOAP_OK;
}

int calcService::prime(double a, double &result) {
  result = 0;
  int n = a;
  if (n < 2)
    return SOAP_OK;

  if (n == 2) {
    result = 1;
    return SOAP_OK;
  }
  for (int i = 2; i * i <= n; i++)
    if (n % i == 0)
      return SOAP_OK;
  result = 1;
  return SOAP_OK;
}

double factorial2(int a) { return 0; }

int calcService::factorial(double a, double &result) {
  result = factorial2(a);
  return SOAP_OK;
}
